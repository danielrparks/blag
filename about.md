---
layout: page
title: About
permalink: /about/
---

I realized I had some cool stuff to write about, so I did.


This blog is generated using Jekyll: [jekyllrb.com](https://jekyllrb.com/)

I'm using a very slightly modified version of the Minima theme:
[jekyll][jekyll-organization] /
[minima](https://github.com/jekyll/minima)

You can find the source code for Jekyll at GitHub:
[jekyll][jekyll-organization] /
[jekyll](https://github.com/jekyll/jekyll)

[jekyll-organization]: https://github.com/jekyll
