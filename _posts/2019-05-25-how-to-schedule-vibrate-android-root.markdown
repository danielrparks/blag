---
layout: post
title: How to schedule ringer mode/vibrate on a rooted Android device
categories: [tech, android, dev]
tags: [vibrate, android, root, ringer, alarm, cron, schedule, adb]
date: 2019-05-25 19:42:45 -0500
---

One of the biggest disappointments I experienced when I bought my first up-to-date Android device was that despite all of the fancy settings it had for scheduling what were essentially different kinds of silence, it didn't support scheduling ringer modes. See, what I really wanted was for it to vibrate only during the school day, and make regular notification sounds the rest of the time. That was Android 8.0 Oreo, and with Android Q nearing launch, it doesn't look like Google plans to change that anytime soon.

Time to take matters into my own hands.

## Android App Spotlight: crond
crond is an app that emulates the Unix cron utility using `AlarmManager`. For the uninitiated, cron is a service on Unix/Linux systems that allows users to schedule actions to repeat at certain intervals. Because it uses `AlarmManager` to schedule the events, the same API that your regular, you guessed it, _alarm_ apps use, its impact on battery life is negligible. I used crond for this hack because I didn't want to write an app myself (and I would use `AlarmManager` anyway if I did, so it's basically perfect).

[<img src="https://f-droid.org/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/app/it.faerb.crond)

You can download it directly from the f-droid website, but if you don't have the f-droid app, I highly recommend it. F-droid is dedicated to creating an open-source app store for Android, where all of the apps are open source as well. There are tons of really nice utilities on f-droid and also some apps that you can find on the Google Play Store, except with all of the premium features enabled for free _by the developer_.

## crontab Syntax Primer
crond normally reads from a file called `crontab`, which contains the timing information and the commands that it should run at those times. On a \*nix system, this file would be at `/etc/crontab`. For the crond Android app, it's at `/data/crontab`, so you'll also need a text editor that can use root permissions. I'm using [Turbo Editor](http://vmihalachi.github.io/turbo-editor/).

crontab syntax consists of 6 fields, separated by spaces. The last field includes all characters to the end of the line, including any remaining spaces.

1. Minute [0,59]
2. Hour [0,23]
3. Day of the month [1,31]
4. Month of the year [1,12]
5. Day of the week ([0,6] with 0=Sunday)
6. The command to run

The crond app will also give you a detailed description of when the command will trigger so that you can make sure you did it right.

## Enabling/Disabling Vibrate Mode from the Command Line
This is the tough one.

The ringer mode setting is controlled through the [`AudioManager` API](https://developer.android.com/reference/android/media/AudioManager). We can control services through the appropriately named Android command `service`, but that presents more problems:

1. The command syntax is complicated and inconsistent across devices and Android versions.
2. There is no `AudioManager` service.

### service call Syntax
`service` allows us to call Java methods of services running on Android, but there's a catch: we have to know the _number_ of the method.

The syntax to the `service call` command is as follows:

{% highlight shell %}
service call [service name] [method number] [arguments...]
{% endhighlight %}

The arguments to the method are notated by the type and the value. For example, if we wanted to call a method that required two integers, say 4 and 2, the arguments would be:

`i32 4 i32 2`

That's because integers in Java are 32 bits wide.

### Calling a real method

So, the AudioManager source code is part of the [Android framework](https://android.googlesource.com/platform/frameworks/base/) source. Choose your Android version from the list of Branches on the left (mine is `oreo-release`) and navigate to `media / java / android / media / AudioManager.java`.

Find the method `setRingerMode(int ringerMode)`. This is the method used by Android apps when they change the ringer mode. The Oreo one looks like this:

{% highlight java %}
public void setRingerMode(int ringerMode) {
		if (!isValidRingerMode(ringerMode)) {
				return;
		}
		final IAudioService service = getService();
		try {
				service.setRingerModeExternal(ringerMode, getContext().getOpPackageName());
		} catch (RemoteException e) {
				throw e.rethrowFromSystemServer();
		}
}
{% endhighlight %}

Now we're getting somewhere! `AudioService` _is_ available from `service call`. The `Context` used here is the context of the app that called `AudioManager`, and the method `getOpPackageName()` returns the package name _as a string_ for use with AppOps (Android's permissions management system). With this information, we can infer that it's being used as a permissions check to determine whether or not the app that called `setRingerMode(int ringerMode)` is allowed to set the ringer mode to that value.

Now, let's find the method `void setRingerModeExternal(int ringerMode, String caller)`. Navigate to `media / java / android / media / IAudioService.aidl`, and, I kid you not, _count_ the number of methods up to and including the one you want to call. For Oreo, and I'd like to stress that this _can_ change between Android releases, `setRingerModeExternal()` is the 13<sup>th</sup> method.

So, let's put together our command:
 - The service is named `audio`, according to the output of the command `service list`
 - `setRingerModeExternal()` is the 13<sup>th</sup> method
 - According to the [`AudioManager` documentation](https://developer.android.com/reference/android/media/AudioManager), the parameter `ringerMode` should be 1 for vibrate or 2 for normal sounds
 - ?

Wait, what do we use for the `caller` parameter? This was really interesting, because the first time I tried to do this, I gave up here. As it turns out, since it's used by `AudioManager` for permission checks and we're bypassing `AudioManager` entirely, we can set it to _whatever we want_. Well, not really. It has to be an actual package on the system. I set it to `com.android.systemui`, since the user interface is definitely allowed to change its own volume.

Let's test it with `adb`:

{% highlight shell %}
adb shell service call audio 13 i32 1 s16 com.android.systemui
{% endhighlight %}

Success! My phone is now set to vibrate. If this did not work for you, make sure you have the correct method number and that the parameters are correct. Also keep in mind that from Nougat onwards, setting it to 0 (Silent) requires additional permissions, not that you would need to since Android lets you schedule Do Not Disturb.

## Putting it all together
I want to turn on vibrate only during school days, so that's 9:00 AM to 4:15 PM, Monday through Friday, August through May. That translates to the following two crontab lines:

{% highlight crontab %}
0 9 * 1-5,8-12 1-5 service call audio 13 i32 1 s16 com.android.systemui
15 16 * 1-5,8-12 1-5 service call audio 13 i32 1 s16 com.android.systemui
{% endhighlight %}

The first line schedules enabling vibrate at the beginning of the day, and the second schedules disabling it for the end of the day.

![screenshot of crond]({{'/assets/Screenshot_20190525-193235.png' | relative_url}})

Mission accomplished!

## Conclusion

The crond app is a great way for hacking together automation solutions if you don't want the battery drain from other automation apps, but the catch is that you need technical knowledge.

On the bright side, you get to learn a lot about how Android works under the hood. That's one of the reasons why I like Android so much: it's very customizable and the end user is allowed to truly understand its workings.

As for this specific issue, I should probably just make an app for it. This could definitely be done without needing root.
