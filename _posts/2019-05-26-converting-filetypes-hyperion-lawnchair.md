---
layout: post
title: "Converting filetypes: migrating from hyperion to Lawnchair"
categories: [tech, filetypes]
tags: [android, database, launcher, hyperion, lawnchair, adb, linux, zip]
date: 2019-05-26 16:41:37 -0500
---

[hyperion](https://play.google.com/store/apps/details?id=projekt.launcher) and [Lawnchair](https://lawnchair.app/) are both launcher apps based on the Google Pixel Launcher that intend to bring it to all devices. Their user interface is pretty similar, and I recently decided to switch back to Lawnchair because v2 is coming closer to release and the alpha builds look _amazing_. Also, hyperion is getting slower and I really just don't want to deal with its IAP paywalls (or random database corruption glitches) anymore.

Anyway, I want to keep my launcher layout. Sounds simple enough, right? I'll just create a backup from hyperion and then import it into Lawnchair.

<div class="figcontainer">
	<figure class="image" style="width:50%">
		<img src="{{ '/assets/Screenshot_20190526-094242.png' | relative_url }}">
		<figcaption>Nope.</figcaption>
	</figure>
	<figure class="image">
		<img src="{{ '/assets/Screenshot_20190526-102123.png' | relative_url }}">
		<figcaption>The alpha build of Lawnchair v2 handles this a little better.</figcaption>
	</figure>
</div>

## Investigating
I created a backup in Lawnchair also so we can inspect the file structure.

Let's grab both files using adb:

{% highlight plaintext %}
» adb pull /sdcard/hyperion/backups/May\ 26,\ 2019.backup
» adb pull /sdcard/Documents/Lawnchair/backup/May\ 26,\ 2019\ 09:45:03.shed
{% endhighlight %}

Inspect them using `file`:

{% highlight plaintext %}
» file *
May 26, 2019.backup:        Zip archive data, at least v2.0 to extract
May 26, 2019 09:45:03.shed: Zip archive data, at least v2.0 to extract
{% endhighlight %}

OK, seems the same so far. Let's extract them:

{% highlight plaintext %}
» unzip May\ 26,\ 2019.backup -d hyperion
Archive:  May 26, 2019.backup
  inflating: hyperion/launcher.db
  inflating: hyperion/app_icons.db
  inflating: hyperion/preview.png
  inflating: hyperion/wallpaper.png
  inflating: hyperion/widgetpreviews.db
  inflating: hyperion/projekt.launcher_preferences.xml
» unzip May\ 26,\ 2019\ 09:45:03.shed -d lawnchair
Archive:  May 26, 2019 09:45:03.shed
  inflating: lawnchair/lcbkp
  inflating: lawnchair/wallpaper.png
  inflating: lawnchair/launcher.db
  inflating: lawnchair/ch.deletescape.lawnchair.plah_preferences.xml
{% endhighlight %}

Seems fine so far. The backups include app preferences, which are obviously going to be incompatible, so I won't bother trying to translate them. hyperion has the extra `app_icons.db` because I applied an icon pack. `lcbkp` is metadata in JSON format. I won't bother to edit this because it's basically just the time of creation as far as I'm concerned. I'm not sure about the rest, but I'm pretty sure that Lawnchair doesn't need them.

The launcher databases are both `sqlite3` databases, so let's open them:

{% highlight plaintext %}
» sqlite3 launcher.db
{% endhighlight %}

The problem becomes apparent when looking at the structure of the `favorites` table:

<div class="sidebyside" style="max-height:16em;">
	<div class="container codewrap">
		<strong>hyperion</strong>
{% highlight sql %}
sqlite> .tables
android_metadata  favorites         workspaceScreens
sqlite> .schema favorites
CREATE TABLE favorites (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,screen INTEGER,cellX INTEGER,cellY INTEGER,spanX INTEGER,spanY INTEGER,itemType INTEGER,appWidgetId INTEGER NOT NULL DEFAULT -1,iconPackage TEXT,iconResource TEXT,icon BLOB,appWidgetProvider TEXT,modified INTEGER NOT NULL DEFAULT 0,restored INTEGER NOT NULL DEFAULT 0,profileId INTEGER DEFAULT 0,rank INTEGER NOT NULL DEFAULT 0,options INTEGER NOT NULL DEFAULT 0, titleAlias TEXT, customIcon BLOB, iconName TEXT, iconPackName TEXT, isFolderMask INTEGER NOT NULL DEFAULT 0, folderMaskInfoComponent TEXT, itemHotseatPageId INTEGER NOT NULL DEFAULT 0, showFolderMaskIcon INTEGER NOT NULL DEFAULT 0, sesameShortcut INTEGER NOT NULL DEFAULT 0, sesameShortcutCn TEXT);
{% endhighlight %}
	</div>
	<div class="container codewrap">
		<strong>Lawnchair</strong>
{% highlight sql %}
sqlite> .tables
android_metadata  favorites         workspaceScreens
sqlite> .schema favorites
CREATE TABLE favorites (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,screen INTEGER,cellX INTEGER,cellY INTEGER,spanX INTEGER,spanY INTEGER,itemType INTEGER,appWidgetId INTEGER NOT NULL DEFAULT -1,iconPackage TEXT,iconResource TEXT,icon BLOB,customIcon BLOB,titleAlias TEXT,appWidgetProvider TEXT,modified INTEGER NOT NULL DEFAULT 0,restored INTEGER NOT NULL DEFAULT 0,profileId INTEGER DEFAULT 0,rank INTEGER NOT NULL DEFAULT 0,options INTEGER NOT NULL DEFAULT 0);
{% endhighlight %}
	</div>
</div>

## Fixing the problem
This seems pretty simple; all I have to do is remove some columns from the hyperion database. While I'm at it, I'll also make sure they're in the same order as Lawnchair's.

<div class="codewrap">
{% highlight sql %}
sqlite> begin transaction;
sqlite> alter table favorites rename to favorites_temp
   ...> ;
sqlite> CREATE TABLE favorites (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,screen INTEGER,cellX INTEGER,cellY INTEGER,spanX INTEGER,spanY INTEGER,itemType INTEGER,appWidgetId INTEGER NOT NULL DEFAULT -1,iconPackage TEXT,iconResource TEXT,icon BLOB,customIcon BLOB,titleAlias TEXT,appWidgetProvider TEXT,modified INTEGER NOT NULL DEFAULT 0,restored INTEGER NOT NULL DEFAULT 0,profileId INTEGER DEFAULT 0,rank INTEGER NOT NULL DEFAULT 0,options INTEGER NOT NULL DEFAULT 0);
sqlite> insert into favorites
   ...> select _id, title, intent, container, screen, cellX, cellY, spanX, spanY, itemType, appWidgetId, iconPackage, iconResource, icon, customIcon, titleAlias, appWidgetProvider, modified, restored, profileId, rank, options from favorites_temp;
sqlite> drop table favorites_temp;
sqlite> commit;
{% endhighlight %}
</div>

Now, the favorites table from hyperion has the same structure as the one from Lawnchair.

<div class="codewrap">
{% highlight sql %}
sqlite> .schema favorites
CREATE TABLE favorites (_id INTEGER PRIMARY KEY,title TEXT,intent TEXT,container INTEGER,screen INTEGER,cellX INTEGER,cellY INTEGER,spanX INTEGER,spanY INTEGER,itemType INTEGER,appWidgetId INTEGER NOT NULL DEFAULT -1,iconPackage TEXT,iconResource TEXT,icon BLOB,customIcon BLOB,titleAlias TEXT,appWidgetProvider TEXT,modified INTEGER NOT NULL DEFAULT 0,restored INTEGER NOT NULL DEFAULT 0,profileId INTEGER DEFAULT 0,rank INTEGER NOT NULL DEFAULT 0,options INTEGER NOT NULL DEFAULT 0);
{% endhighlight %}
</div>

Now, let's stick the hyperion database into the Lawnchair backup:

{% highlight plaintext %}
» cd hyperion/
» zip ../May\ 26,\ 2019\ 09:45:03.shed launcher.db
updating: launcher.db
	zip warning: Local Entry CRC does not match CD: launcher.db
 (deflated 13%)
» cd ..
{% endhighlight %}

Push it back to the phone:

{% highlight plaintext %}
» adb push May\ 26,\ 2019\ 09:45:03.shed /sdcard/Documents/Lawnchair/backup/
{% endhighlight %}

I got a nice "Restore Success" message, but the restoration was clearly not a success. The layout is the same as before.


## More investigating

After some messing around, I can reasonably assume that it is silently failing, because it restores the default layout when importing this database. (There's also the fact that it crashes the alpha build of v2. That might have clued me in a little.)

Let's try to remove anything from the database that could cause the restore to fail.

The first thing that sticks out to me is that I had a widget on my homescreen and that's in the database.

<div class="codewrap">
{% highlight sql %}
sqlite> select * from favorites where title is null;
_id|title|intent|container|screen|cellX|cellY|spanX|spanY|itemType|appWidgetId|iconPackage|iconResource|icon|customIcon|titleAlias|appWidgetProvider|modified|restored|profileId|rank|options
109|||-100|0|0|0|5|1|4|9||||||com.google.android.googlequicksearchbox/com.google.android.apps.gsa.staticplugins.smartspace.widget.SmartspaceWidgetProvider|1555256041267|0|0|0|0
{% endhighlight %}
</div>

Since widgets are a huge compatibility problem, I deleted it.

{% highlight sql %}
sqlite> delete from favorites where title is null;
{% endhighlight %}

The next thing I did was set the icons to null in case the two launchers used different bitmap formats for some reason (this is fine; lawnchair can regenerate them).

{% highlight sql %}
sqlite> begin transaction;
sqlite> update favorites set icon = NULL;
sqlite> update favorites set customIcon = NULL;
sqlite> commit;
{% endhighlight %}

Then I noticed that according to `file`, the user version of the hyperion sqlite database was set to 33 while the lawnchair one was set to 29. I fixed that too.

{% highlight sql %}
sqlite> PRAGMA user_version = 29;
{% endhighlight %}

**... and _that_ was it**. Sometimes grasping at straws does pay off.

I later went back and redid only deleting the extra columns and setting the correct user version, and Lawnchair still accepted it, so those were really the only two things I needed to do.

Although... Remember how I said that the widget was a problem? Yeah, it didn't survive the transfer even though I didn't delete it this time. It _was_ incompatible after all, but Lawnchair handled it correctly and simply deleted it.
